# El bug invisible: La arquitectura


## Ejemplo

Tenemos que hacer un sistema para automatizar la búsqueda de vulnerabilidades
de un sistema SAP


## Casos de uso
1. Escaneo bajo demanda:

> Dado un sistema objetivo  
> Y una lista de modulos  
> Cuando ejecuto un escaneo  
> Entonces el sistema devuelve una lista de vulnerabilidades
