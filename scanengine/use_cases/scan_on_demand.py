import logging

from datetime import datetime
from typing import Iterable

from scanengine.core.entities import Module, ScanResult, ScanStatus, System
from scanengine.core.knowledge_base import KnowledgeBase
from scanengine.core.scanner import Scanner


logger = logging.getLogger(__name__)


class ScanOnDemand:
    """Scan a SAP system on demand"""

    def __init__(
        self,
        knowledge_base: KnowledgeBase,
        scanner: Scanner,
        system: System,
        module_ids: Iterable[str],
    ):
        self.knowledge_base = knowledge_base
        self.scanner = scanner
        self.system = system
        self.module_ids = module_ids

    def execute(self) -> Iterable[ScanResult]:
        results = (self._execute_module(module_id) for module_id in self.module_ids)
        return results

    def _execute_module(self, module_id: str) -> ScanResult:
        started = datetime.utcnow()
        try:
            module = self.knowledge_base[module_id]
            status = self.scanner.scan(self.system, module)
            return ScanResult(
                system=self.system,
                module=module,
                status=status,
                started=started,
                finished=datetime.utcnow(),
            )
        except KeyError:
            return self._handle_module_doesnt_exists(module_id, started)
        except Exception:
            return self._handle_general_error(module, started)

    def _handle_general_error(self, module, started):
        logger.exception("Error running module %r", module.id)
        return ScanResult(
            system=self.system,
            module=module,
            status=ScanStatus.ERROR,
            started=started,
            finished=datetime.utcnow(),
        )

    def _handle_module_doesnt_exists(self, module_id, started):
        logger.error("Module %r does not exists", module_id)
        return ScanResult(
            system=self.system,
            module=create_non_existent_module(module_id),
            status=ScanStatus.ERROR,
            started=started,
            finished=datetime.utcnow(),
        )


def create_non_existent_module(module_id):
    return Module(id=module_id, name="MODULE NOT FOUND", solution="", code="")
