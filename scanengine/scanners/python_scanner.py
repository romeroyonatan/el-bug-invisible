"""This scanner uses python as module language"""
import importlib

from scanengine.core.entities import Module, ScanStatus, System
from scanengine.core.scanner import Scanner


class PythonScanner(Scanner):
    def scan(self, target: System, module: Module) -> ScanStatus:
        """Scan target system using the specified module"""
        executable_module = _load_python_module(module)
        return executable_module.run(target)


def _load_python_module(module: Module):
    my_spec = importlib.util.spec_from_loader(module.id, loader=None)
    my_module = importlib.util.module_from_spec(my_spec)
    exec(module.code, my_module.__dict__)  # pylint: disable=exec-used
    return my_module
