import sys
from typing import Iterable

from scanengine.core.entities import System
from scanengine.knowledge_bases import FilesystemKB
from scanengine.scanners import PythonScanner
from scanengine.serializers.scan_result import ScanResultJSONSerializer
from scanengine.use_cases.scan_on_demand import ScanOnDemand


def run_scan(
    modules: Iterable[str], system: str, knowledge_base: str, output=sys.stdout
):
    knowledge_base = load_knowledge_base(knowledge_base)
    system = System(hostname=system)
    scanner = PythonScanner()
    scan_result = ScanOnDemand(knowledge_base, scanner, system, modules).execute()
    write_report(scan_result, output)


def load_knowledge_base(knowledge_base):
    return FilesystemKB(knowledge_base)


def write_report(scan_result, output):
    report = ScanResultJSONSerializer.encode(scan_result)
    output.write(report)


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("modules", help="ID of modules to use for scan", nargs="+")
    parser.add_argument("-t", "--target", help="Target system to scan", required=True)
    parser.add_argument(
        "-k", "--kb", help="Knowledge base of vulnerabilities", required=True
    )

    args = parser.parse_args()
    run_scan(args.modules, args.target, args.kb)


if __name__ == "__main__":
    main()
