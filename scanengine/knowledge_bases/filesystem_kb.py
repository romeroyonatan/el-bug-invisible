import importlib
import importlib.util
import pathlib
from typing import Dict

from scanengine.core.knowledge_base import KnowledgeBase
from scanengine.core.entities import Module


class FilesystemKB(KnowledgeBase):
    def __init__(self, modules_path):
        self.modules = _load_modules_from_directory(modules_path)

    def __getitem__(self, module_id):
        return self.modules[module_id]


def _load_modules_from_directory(modules_path) -> Dict[str, Module]:
    modules_path = pathlib.Path(modules_path).glob("*.py")
    modules = (_load_module_from_path(path) for path in modules_path)
    return {module.id: module for module in modules}


def _load_module_from_path(path: pathlib.Path) -> Module:
    py_module = _load_python_module_from_file(str(path))
    code = path.read_text()
    return Module(
        id=py_module.ID, name=py_module.NAME, solution=py_module.SOLUTION, code=code
    )


def _load_python_module_from_file(filepath: str):
    loader = importlib.machinery.SourceFileLoader("mod", filepath)
    spec = importlib.util.spec_from_loader(loader.name, loader)
    py_module = importlib.util.module_from_spec(spec)
    loader.exec_module(py_module)

    return py_module
