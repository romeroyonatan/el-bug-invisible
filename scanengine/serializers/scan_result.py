import json

from scanengine.core.entities import ScanStatus


class ScanResultJSONSerializer:
    @staticmethod
    def encode(results):
        return json.dumps(
            [_serialize_module(result_module) for result_module in results]
        )


def _serialize_module(result_module):
    data = {
        "system": result_module.system.hostname,
        "module": result_module.module.id,
        "status": result_module.status.value,
        "started": result_module.started.isoformat(),
        "finished": result_module.finished.isoformat(),
    }
    if result_module.status == ScanStatus.VULNERABLE:
        data["solution"] = result_module.module.solution
    return data
