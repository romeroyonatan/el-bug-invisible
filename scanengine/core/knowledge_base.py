import abc


class KnowledgeBase(abc.ABC):
    @abc.abstractmethod
    def __getitem__(self, module_id):
        """Get the module based on its ID"""
