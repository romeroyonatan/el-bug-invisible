import abc

from .entities import Module, ScanStatus, System


class Scanner(abc.ABC):
    @abc.abstractmethod
    def scan(self, target: System, module: Module) -> ScanStatus:
        """Scan target system using the specified module"""
