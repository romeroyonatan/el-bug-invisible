import dataclasses
import datetime
import enum


@dataclasses.dataclass
class System:
    hostname: str


@dataclasses.dataclass
class Module:
    id: str
    code: str
    name: str
    solution: str


class ScanStatus(enum.Enum):
    VULNERABLE = "VULNERABLE"
    NOT_VULNERABLE = "NOT_VULNERABLE"
    NOT_APPLICABLE = "NOT_APPLICABLE"
    ERROR = "ERROR"


@dataclasses.dataclass
class ScanResult:
    system: System
    module: Module
    status: ScanStatus
    started: datetime.datetime
    finished: datetime.datetime
