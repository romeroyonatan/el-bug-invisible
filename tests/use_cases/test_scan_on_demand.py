from scanengine.core.entities import Module, ScanStatus, System
from scanengine.core.scanner import Scanner
from scanengine.use_cases.scan_on_demand import ScanOnDemand


def create_module(id="TEST_ID"):
    return Module(id=id, code="", name="Test module", solution="")


A_SYSTEM = System(hostname="example.com")
NOT_USED = None


def test_not_vulnerability_found():
    knowledge_base = {"SAP_001": create_module("SAP_001")}
    scanner = FakeScanner(ScanStatus.NOT_VULNERABLE)
    use_case = ScanOnDemand(knowledge_base, scanner, A_SYSTEM, module_ids=["SAP_001"])

    results = use_case.execute()

    scan_result = next(results)

    assert scan_result.system == A_SYSTEM
    assert scan_result.module == knowledge_base["SAP_001"]
    assert scan_result.status == ScanStatus.NOT_VULNERABLE


def test_vulnerability_found():
    knowledge_base = {"SAP_001": create_module("SAP_001")}
    scanner = FakeScanner(ScanStatus.VULNERABLE)
    use_case = ScanOnDemand(knowledge_base, scanner, A_SYSTEM, module_ids=["SAP_001"])

    results = use_case.execute()

    scan_result = next(results)

    assert scan_result.system == A_SYSTEM
    assert scan_result.module == knowledge_base["SAP_001"]
    assert scan_result.status == ScanStatus.VULNERABLE


def test_module_does_not_exists():
    knowledge_base = {}
    scanner = FakeScanner(NOT_USED)
    use_case = ScanOnDemand(knowledge_base, scanner, A_SYSTEM, module_ids=["SAP_001"])

    results = use_case.execute()

    scan_result = next(results)

    assert scan_result.system == A_SYSTEM
    assert scan_result.module.id == "SAP_001"
    assert scan_result.status == ScanStatus.ERROR


class FakeScanner(Scanner):
    def __init__(self, status: ScanStatus):
        self.status = status

    def scan(self, target, module):
        return self.status
