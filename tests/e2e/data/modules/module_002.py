"""This module never reports a vulnerability"""
from scanengine.core.entities import ScanStatus

ID = "TEST_002"
NAME = "Not vulnerable system test"
SOLUTION = """This solution should be never reported

This is a test purpuse module.
"""


def run(system):
    return ScanStatus.NOT_VULNERABLE
