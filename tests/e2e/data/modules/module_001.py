"""This module always reports a vulnerability"""
from scanengine.core.entities import ScanStatus

ID = "TEST_001"
NAME = "Vulnerable system test"
SOLUTION = """You cannot resolve this vulnerability.

This is a test purpuse module.
"""


def run(system):
    return ScanStatus.VULNERABLE
