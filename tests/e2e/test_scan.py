import unittest

from .runner import ApplicationRunner

VULNERABLE = "TEST_001"
NOT_VULNERABLE = "TEST_002"


class ScanE2ETests(unittest.TestCase):
    def setUp(self):
        self.application = ApplicationRunner()

    def test_scan_report_status(self):
        self.application.run_scan(modules=[VULNERABLE, NOT_VULNERABLE])

        self.application.assert_module_is_reported_as_vulnerable(VULNERABLE)
        self.application.assert_module_is_reported_as_not_vulnerable(NOT_VULNERABLE)

    def test_solution_is_displayed_when_module_is_vulnerable(self):
        self.application.run_scan(modules=[VULNERABLE])

        self.application.assert_solution_is_displayed(VULNERABLE)

    def test_solution_is_not_displayed_when_module_is_not_vulnerable(self):
        self.application.run_scan(modules=[NOT_VULNERABLE])

        self.application.assert_solution_is_not_displayed(NOT_VULNERABLE)
