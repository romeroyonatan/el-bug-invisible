import io
import json
from pathlib import Path

from scanengine import cli
from scanengine.core.entities import ScanStatus

E2E_KNOWLEDGE_BASE = Path(__file__).parent / "data" / "modules"
A_SYSTEM = "localhost"


class ApplicationRunner:
    def __init__(self):
        self.report = None

    def run_scan(self, modules):
        output = io.StringIO()

        cli.run_scan(
            modules=modules,
            system=A_SYSTEM,
            knowledge_base=E2E_KNOWLEDGE_BASE,
            output=output,
        )

        self.report = ScanReportParser(output.getvalue())

    def assert_module_is_reported_as_vulnerable(self, module_id):
        status = self.get_report(module_id)["status"]
        assert (
            status == ScanStatus.VULNERABLE.value
        ), f"Module {module_id!r} expected to be vulnerable, but it has status {status!r}"

    def assert_module_is_reported_as_not_vulnerable(self, module_id):
        status = self.get_report(module_id)["status"]
        assert (
            status == ScanStatus.NOT_VULNERABLE.value
        ), f"Module {module_id!r} expected to be not vulnerable, but it has status {status!r}"

    def assert_solution_is_displayed(self, module_id):
        error_msg = f"Module {module_id!r} expected to have solution, but it doesn't"
        try:
            solution = self.get_report(module_id)["solution"]
            assert solution, error_msg
        except KeyError:
            raise AssertionError(error_msg)

    def assert_solution_is_not_displayed(self, module_id):
        report = self.get_report(module_id)
        assert (
            "solution" not in report
        ), f"Module {module_id!r} not expected to display a solution, but it does"

    def get_report(self, module_id):
        try:
            return self.report[module_id]
        except KeyError:
            raise AssertionError(
                f"Module {module_id!r} is expected to be present in scan's "
                "report but it is not present."
            )


class ScanReportParser:
    def __init__(self, run_scan_output: str):
        report = json.loads(run_scan_output)
        self.data = {module_report["module"]: module_report for module_report in report}

    def __getitem__(self, module_id):
        return self.data[module_id]
