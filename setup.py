from setuptools import setup

setup(
    name="Scan engine",
    version="0.1",
    packages=("scanengine",),
    entry_points={"console_scripts": ["run_scan = scanengine.cli.run_scan:main"]},
)
